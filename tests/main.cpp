#include <wheels/test/test_framework.hpp>

#include <refer/ref.hpp>
#include <refer/ref_counted.hpp>
#include <refer/unmanaged.hpp>
#include <refer/manual.hpp>

using refer::IManaged;
using refer::Ref;
using refer::RefCounted;
using refer::Unmanaged;
using refer::New;
using refer::Adopt;
using refer::AdoptRef;
using refer::ManualLifetime;

//////////////////////////////////////////////////////////////////////

class Robot : public RefCounted<Robot> {
 public:
  Robot(std::string name)
      : name_(std::move(name)) {
    std::cout << "I am alive!" << std::endl;
  }

  const std::string& Name() const {
    return name_;
  }

  void Talk() {
    std::cout << "My name is " << name_ << std::endl;
  }

  ~Robot() {
    std::cout << "Robot " << name_ << " destroyed" << std::endl;
  }

  Ref<Robot> Me() {
    return RefFromThis();
  }

 private:
  std::string name_;
};

//////////////////////////////////////////////////////////////////////

struct X : virtual IManaged {
  ~X() {
    std::cout << "~X" << std::endl;
  }
};

struct Y : X, RefCounted<Y> {
  ~Y() {
    std::cout << "~Y" << std::endl;
  }
};

//////////////////////////////////////////////////////////////////////

class UnmanagedCat : public refer::Unmanaged {
 public:
  UnmanagedCat(std::string name) : name_(name) {
  }

  std::string Name() const {
    return name_;
  }

 private:
  std::string name_;
};

//////////////////////////////////////////////////////////////////////

TEST_SUITE(Ref) {
  SIMPLE_TEST(JustWorks) {
    Ref<Robot> r = New<Robot>("Jake");

    ASSERT_TRUE(r.IsValid());

    r->Talk();
    ASSERT_EQ((*r).Name(), "Jake");
  }

  SIMPLE_TEST(FromRawPointer) {
    Robot* raw_ptr = new Robot("Jake");
    Ref<Robot> r{raw_ptr, AdoptRef{}};

    ASSERT_TRUE(r.IsValid());

    ASSERT_EQ(r->GetRefCount(), 1);
  }

  SIMPLE_TEST(Invalid) {
    Ref<Robot> r{};
    ASSERT_FALSE(r.IsValid());
    ASSERT_EQ(r.Get(), nullptr);
  }

  SIMPLE_TEST(CopyCtor) {
    Ref<Robot> r1 = New<Robot>("Jake");
    Ref<Robot> r2(r1);

    ASSERT_TRUE(r1.IsValid());
    ASSERT_TRUE(r2.IsValid());

    ASSERT_EQ(r1->Name(), "Jake");
    ASSERT_EQ(r2->Name(), "Jake");
  }

  SIMPLE_TEST(MoveCtor) {
    Ref<Robot> r1 = New<Robot>("Jake");

    ASSERT_TRUE(r1.IsValid());

    Ref<Robot> r2(std::move(r1));

    ASSERT_FALSE(r1.IsValid());

    ASSERT_TRUE(r2.IsValid());
    ASSERT_EQ(r2->Name(), "Jake");
  }

  SIMPLE_TEST(Assigment) {
    Ref<Robot> r1;

    ASSERT_FALSE(r1.IsValid());

    {
      Ref<Robot> r2 = New<Robot>("Bob");
      r1 = r2;
    }

    ASSERT_TRUE(r1.IsValid());
    ASSERT_EQ(r1->Name(), "Bob");
  }

  SIMPLE_TEST(Convert) {
    Ref<Y> ry = New<Y>();
    Ref<X> rx{ry};
  }

  SIMPLE_TEST(MoveConvert) {
    Ref<Y> ry = New<Y>();
    Ref<X> rx{std::move(ry)};

    ASSERT_TRUE(!ry.IsValid());
  }

  SIMPLE_TEST(Release) {
    Ref<Robot> r = New<Robot>("Jake");

    Robot* raw_ptr = r.Release();

    ASSERT_FALSE(r.IsValid());

    ASSERT_EQ(raw_ptr->Name(), "Jake");
    raw_ptr->ReleaseRef();
  }

  SIMPLE_TEST(New) {
    auto ref = New<Robot>("Mike");
    ASSERT_TRUE(ref.IsValid());
    ASSERT_EQ(ref->Name(), "Mike");
  }

  SIMPLE_TEST(Adopt) {
    auto* raw_ptr = new Robot("Bob");

    ASSERT_EQ(raw_ptr->GetRefCount(), 1);
    auto ref = Adopt(raw_ptr);
    ASSERT_EQ(ref->GetRefCount(), 1);
  }

  SIMPLE_TEST(RefFromThis) {
    auto ref1 = New<Robot>("Mary");
    auto ref2 = ref1->RefFromThis();

    ASSERT_EQ(ref1->GetRefCount(), 2);
    ASSERT_EQ(ref2->Name(), "Mary");
  }

  SIMPLE_TEST(Inlined) {
    ManualLifetime<UnmanagedCat> cat;

    ASSERT_FALSE(cat.Allocated());

    {
      auto ref = cat.Allocate("Fluffy");
      ASSERT_EQ(ref->Name(), "Fluffy");
    }

    ASSERT_TRUE(cat.Allocated());

    {
      auto ref = cat.Get();
      ASSERT_EQ(ref->Name(), "Fluffy");
    }

    cat.Deallocate();

    ASSERT_FALSE(cat.Allocated());
  }

  SIMPLE_TEST(IsManualLifetime) {
    {
      auto ref = New<Robot>("Bender");
      ASSERT_FALSE(ref.IsManualLifetime());
    }

    {
      UnmanagedCat cat("Fluffy");
      auto ref = Adopt(&cat);

      ASSERT_TRUE(ref.IsManualLifetime());
    }
  }
}

RUN_ALL_TESTS()
